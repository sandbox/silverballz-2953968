Commerce Attribute Price
===============

Extends Drupal Commerce Price module with functionality to set price based on calculated attribute values.

## Installation

To install with [Composer](https://getcomposer.org/) type:

```
composer require drupal/commerce_attribute_price:^1.0
```